<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Home work!</h1>

        <p class="lead">Every thing works just fine :)</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">YII framework documentation here</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>All students</h2>

                <p>shows all students entered to database</p>

                <p><a class="btn btn-default" href="http://localhost/basic/web/?r=student/viewall">All students show &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Choose a spesific student via URL</h2>

                <p>shows data from DB regarding a spesific students via entering his ID throguh URL</p>

                <p><a class="btn btn-default" href="http://localhost/basic/web/?r=student/view&id=1">show student via URL (defualt id=1) &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>...</h2>

                <p>....</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
