<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movie`.
 */
class m170606_195944_create_movie_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('movie', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45),
            'genre' => $this->string(45),
            'min_age' => $this->integer(),
            'rank' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movie');
    }
}
